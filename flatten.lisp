(in-package :netfarm-slacker)

(defun flatten-lambdas (expression environment
                        &key (function-position? nil)
                             (tail-position? nil))
  (trivia:ematch expression
    ((list* 'lambda arguments body)
     (let* ((name (gensym "LAMBDA"))
            (position (allocate-function-binding name))
            (new-environment (extend-environment arguments environment)))
       (setf (gethash name *functions*)
             (list arguments environment (flatten-lambdas `(begin ,@body)
                                                          new-environment
                                                          :tail-position? t)))
       (if tail-position?
           `(return (function ,position))
           `(function ,position))))
    
    ((list 'quote value)
     (let ((position (allocate-constant-global-binding
                      (translate-constant value))))
       (if tail-position?
           `(return (global ,position))
           `(global ,position))))
    ((type string)
     (let ((position (allocate-constant-global-binding expression)))
       (if tail-position?
           `(return (global ,position))
           `(global ,position))))

    ((list 'if test then else)
     `(if ,(flatten-lambdas test environment :function-position? nil)
          ,(flatten-lambdas then environment
                            :function-position? function-position?
                            :tail-position? tail-position?)
          ,(flatten-lambdas else environment
                            :function-position? function-position?
                            :tail-position? tail-position?)))

    ((list 'set! variable value)
     `(begin
       (set! ,(find-binding variable environment)
             ,(flatten-lambdas value environment :function-position? nil))
       ,(if tail-position?
            `(return (byte 0))
            `(byte 0))))

    ((list* 'begin expressions)
     `(begin
       ,@(loop for (expression . rest) on expressions
               collect (flatten-lambdas expression environment
                                        :function-position? (and (null rest) function-position?)
                                        :tail-position? (and (null rest) tail-position?)))))
    
    ((cons function arguments)
     (list* (if tail-position? 'tailcall 'call)
            (flatten-lambdas function environment :function-position? t)
            (loop for argument in arguments
                  collect (flatten-lambdas argument environment))))
    
    ((type integer)
     (if (<= 0 expression 255)
         (if tail-position?
             `(return (byte ,expression))
             `(byte ,expression))
         (let ((position (allocate-constant-global-binding expression)))
           (if tail-position?
               `(return (global ,position))
               `(global ,position)))))

    ((or :true :false)
     (flatten-lambdas (list 'quote expression) environment
                      :function-position? function-position?
                      :tail-position? tail-position?))
    
    ((type symbol)
     (let ((binding
             (find-binding expression environment
                           :function-position? function-position?)))
       (if tail-position?
           `(return ,binding)
           binding)))))

(defun flatten-lambdas/toplevel (expression)
  (trivia:ematch expression
    ((cons 'define-macro _))            ; Nothing to do for DEFINE-MACRO
    ((list* 'define (cons name arguments) body)
     (setf (gethash name *functions*)
           (list arguments
                 *empty-environment*
                 (flatten-lambdas `(begin ,@(mapcar #'macroexpand-expression body))
                                  (extend-environment arguments
                                                      *empty-environment*)
                                  :tail-position? t))))
    ((list 'define name (list* 'lambda arguments body))
     (flatten-lambdas/toplevel `(define (,name ,@arguments) ,@body)))
    ((list 'define name (list 'quote value))
     (setf (gethash name *globals*) (translate-constant value)))
    ((list 'define name
           (and value
                (or (type number) (type string) :true :false)))
     (setf (gethash name *globals*) value))
    ((list 'define-method (cons name arguments) body)
     (let ((new-name (gethash (string-downcase name) *methods*)))
       (flatten-lambdas/toplevel `(define (,new-name methods ,@arguments)
                                    ,body)))))
  t)
