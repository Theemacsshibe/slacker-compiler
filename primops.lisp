(in-package :netfarm-slacker)

(defstruct primop
  (name "")
  (argument-count 0)
  (opcode 0))
(defstruct (valueless-primop (:include primop)))
(defstruct n-ary-primop
  (name "")
  (opcode 0))
(defstruct (call-method-primop (:include n-ary-primop)))

(defun find-opcode-by-name (name)
  (or (position name netfarm-scripts::*opcode-data*
                :key #'first :test #'string-equal)
      (error "No opcode named ~a exists" name)))
(defmacro primop-named (name)
  (find-opcode-by-name name))

(defvar *primop-table* (make-hash-table :test 'equal))

(defun find-primop (name)
  (or (gethash (string name) *primop-table*)
      (error "No primop named ~a exists" name)))

(macrolet ((define-primop (constructor name argument-count &key (opcode-name name))
             `(setf (gethash ,(symbol-name name) *primop-table*)
                    (,constructor :name ,(symbol-name opcode-name)
                                  :argument-count ,argument-count
                                  :opcode (find-opcode-by-name ,(symbol-name opcode-name)))))
           (define-primops (&body names)
             `(progn
                ,@(loop for name in names
                        collect `(define-primop make-primop ,@name))))
           (define-valueless-primops (&body names)
             `(progn
                ,@(loop for name in names
                        collect `(define-primop make-valueless-primop ,@name)))))
  (define-primops
    (error 2)
    (cdr 1) (car 1) (cons 2)
    (null? 1 :opcode-name null)
    (cons? 1 :opcode-name consp)
    (integer? 1 :opcode-name integerp)
    (string? 1 :opcode-name stringp)
    (append 2) (equal? 2 :opcode-name equal)
    (+ 2) (- 2) (* 2) (/ 2) (abs 1)
    (= 2) (< 2) (> 2) (/= 2) (<= 2) (>= 2)
    (string-length 1) (string-ref 2) (string-append 1)
    (schema 1)
    (object-value 2) (object-computed-values 2)
    (object-authors 1)
    (object-bound? 2 :opcode-name object-boundp)
    (object-authors 1)
    (query-recommender 4)
    (self 0) (sender 0))
  (define-valueless-primops
    (add-computed-value 2)
    (remove-computed-value 2))
  (setf (gethash "LIST" *primop-table*)
        (make-n-ary-primop :opcode (find-opcode-by-name "LIST")
                           :name "LIST")
        (gethash "CALL-METHOD" *primop-table*)
        (make-call-method-primop :opcode (find-opcode-by-name "CALL-METHOD")
                                 :name "CALL-METHOD")))

(defun primop-name-p (name)
  (nth-value 1 (gethash (symbol-name name) *primop-table*)))
(defun primop-pushes-value-p (primop)
  (not (typep primop 'valueless-primop)))

(defgeneric emit-primop-instruction (primop arguments)
  (:method ((primop primop) arguments)
    (assert (= (primop-argument-count primop) (length arguments))
            ()
            "~a expects ~d argument~:p, but was given ~d"
            (primop-name primop)
            (primop-argument-count primop)
            (length arguments))
    (append (reduce #'append (mapcar #'generate-code arguments)
                    :from-end t)
            (list (primop-opcode primop))))
  (:method ((primop n-ary-primop) arguments)
    (append (reduce #'append (mapcar #'generate-code arguments)
                    :from-end t)
            (list
             (n-ary-primop-opcode primop)
             (length arguments))))
  (:method ((primop call-method-primop) arguments)
    (destructuring-bind (object method-name &rest arguments)
        arguments
      (append (reduce #'append (mapcar #'generate-code arguments)
                      :from-end t)
              (generate-code object)
              (generate-code method-name)
              (list
               (n-ary-primop-opcode primop)
               (length arguments))))))
