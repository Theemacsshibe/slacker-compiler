(in-package :netfarm-slacker)

(defvar *bytecode*)
(defmacro with-bytecode (() &body body)
  `(let ((*bytecode* (make-array 16
                                 :element-type '(unsigned-byte 8)
                                 :initial-element 0
                                 :adjustable t
                                 :fill-pointer 0)))
     ,@body
     *bytecode*))

(defun emit (&rest codes)
  (dolist (code codes)
    (vector-push-extend code *bytecode* 16)))
