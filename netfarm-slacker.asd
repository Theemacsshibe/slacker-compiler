(asdf:defsystem :netfarm-slacker
  :depends-on (:netfarm :alexandria :trivia)
  :serial t
  :components ((:file "package")
               (:file "reader")
               (:file "primops")
               (:file "environments")
               (:file "macroexpand")
               (:file "translate-constant")
               (:file "flatten")
               (:file "codegen")
               (:file "compile")))
