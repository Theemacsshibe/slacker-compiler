(in-package :netfarm-slacker)

(defun macroexpand-expression (expression)
  (typecase expression
    ((cons (eql 'quote)) expression)
    (cons
     (destructuring-bind (name . arguments) expression
       (multiple-value-bind (macro-function present?)
           (find-macro name)
         (if present?
             (macroexpand-expression (funcall macro-function arguments))
             (mapcar #'macroexpand-expression expression)))))
    (t expression)))
