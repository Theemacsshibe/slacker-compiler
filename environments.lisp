(in-package :netfarm-slacker)

;;; Local environments

(defvar *empty-environment* '())

(defun extend-environment (names old-environment)
  (cons (coerce names 'vector) old-environment))

(defun find-local-binding (name environment)
  (loop for frame in environment
        for frame-depth from 0
        for frame-position = (position name frame)
        unless (null frame-position)
          return `(local ,frame-position ,frame-depth)))

;;; The global environments

(defvar *global-environment*)
(defvar *function-environment*)
(defvar *functions*)
(defvar *globals*)
(defvar *methods*)
(defvar *macros*)
(defvar *constants*)

(defmacro with-global-environments (() &body body)
  `(let ((*global-environment* (make-array 256
                                           :initial-element nil
                                           :fill-pointer 0))
         (*function-environment* (make-array 256
                                             :initial-element nil
                                             :fill-pointer 0))
         (*functions* (make-hash-table))
         (*macros*    (make-hash-table))
         (*globals*   (make-hash-table))
         (*methods*   (make-hash-table :test 'equal))
         (*constants* (make-hash-table :test 'equal)))
     ,@body))

(defun allocate-global-binding (name)
  (or (vector-push name *global-environment*)
      (error "out of global bindings"))) 

(defun allocate-function-binding (name)
  (or (vector-push name *function-environment*)
      (error "out of function bindings")))

(defun allocate-constant-global-binding (constant)
  (let ((constant (netfarm:normalise-graph constant)))
    (or (gethash constant *constants*)
        (let ((name (gensym "CONSTANT")))
          (setf (gethash name *globals*) constant)
          (setf (gethash constant *constants*)
                (allocate-global-binding name))))))

(defun find-global-binding (name)
  (alexandria:if-let ((p (position name *global-environment*)))
    `(global ,p)))

(defun find-function-binding (name)
  (alexandria:if-let ((p (position name *function-environment*)))
    `(function* ,p)))

(defun find-binding (name environment &key function-position?)
  (or (find-local-binding name environment)
      (find-function-binding name)
      (find-global-binding name)
      (if (primop-name-p name)
          (if function-position?
              `(primop ,name)
              (find-primop-wrapper name)))
      (error "~s is unbound" name)))

(defun find-primop-wrapper (name)
  (let* ((number (allocate-function-binding name))
         (data   (gethash (symbol-name name) *primop-table*))
         (arguments (loop repeat (primop-argument-count data)
                          collect (gensym "ARGUMENT"))))
    (setf (gethash name *functions*)
          (list arguments *empty-environment*
                `(tailcall (primop ,name)
                           ,@(loop for p below (primop-argument-count data)
                                   collect `(local ,p 0)))))
    `(function* ,number)))

(defun add-macro (name lambda-list body)
  (when (gethash name *macros*)
    (error "There is already a macro named ~a" name))
  (alexandria:with-gensyms (arguments)
    (setf (gethash name *macros*)
          (compile nil
                   `(lambda (,arguments)
                      (destructuring-bind (,lambda-list) (list ,arguments)
                        ,@body))))))

(defun find-macro (name)
  (gethash name *macros*))
