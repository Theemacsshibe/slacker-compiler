def a(m, n):
    if m == 0: return n + 1
    if n == 0: return a(m - 1, 1)
    return a(m - 1, a(m, n - 1))

if __name__ == "__main__":
    import timeit
    ITERATIONS = 10000
    time = timeit.timeit("a(3, 3)", number=ITERATIONS, globals=globals())
    print("A(3, 3) = {}".format(a(3, 3)))
    print("{} microseconds/iteration".format(time / ITERATIONS * 1000000))
