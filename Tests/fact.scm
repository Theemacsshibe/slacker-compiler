;;; Adapted from http://rosettacode.org/wiki/Y_combinator#Scheme

(define (factorial n)
  ((fac) n))

(define (Y f)                       ; (Y f) = (g g) where                        
  ((lambda (g) (g g))               ;         (g g) = (f  (lambda (a) ((g g) a)))
   (lambda (g)                      ; (Y f) ==        (f  (lambda (a) ((Y f) a)))
     (f (lambda (a) ((g g) a))))))

(define (fac)                ; fac = (Y f) = (f      (lambda (a) ((Y f) a))) 
  (Y (lambda (r)             ;     = (lambda (x) ... (r     (- x 1)) ... )
       (lambda (x)           ;        where   r    = (lambda (a) ((Y f) a))
         (if (< x 2)         ;               (r ... ) == ((Y f) ... )
             1               ;     == (lambda (x) ... (fac  (- x 1)) ... )
             (* x (r (- x 1))))))))
