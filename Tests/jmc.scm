(define (eval e a)
  (if (string? e)
      (assoc e a)
      (eval-combination e a)))

(define-macro (cond . body)
  (trivia:match body
    ((cl:list (cl:list 'else value)) value)
    ((cl:list (cl:list test  value))
     `(if ,test
          ,value
          (error "exhausted COND" #f)))
    ((cl:list* (cl:list test value) rest)
     `(if ,test
          ,value
          (cond ,@rest)))))

(define-macro (case value . body)
  (trivia:match body
    ('() #f)
    ((cl:list (cl:list 'else body)) body)
    ((cl:list* (cl:list case body) rest)
     `(if (equal? ',case ,value)
          ,body
          (case ,value ,@rest)))))

(define (and x y)
  (if (null? x)
      #f
      (null? y)))

(define (not x)
  (null? x))

(define (pair x y)
  (if (null? x)
      '()
      (cons (cons (car x) (car y))
            (pair (cdr x) (cdr y)))))

(define (assoc x l)
  (if (equal? (car (car l)) x)
      (cdr (car l))
      (assoc x (cdr l))))

(define (lisp-wrap boolean)
  (if boolean "t" '()))
(define (second x) (car (cdr x)))
(define (third x)  (car (cdr (cdr x))))

(define (eval-combination e a)
  (cond
   ((string? (car e))
    (case (car e)
      ("quote" (second e))
      ("atom"  (lisp-wrap (string? (eval (second e) a))))
      ("eq"    (lisp-wrap (equal?  (eval (second e) a)
                                   (eval (third  e) a))))
      ("car"   (car  (eval (second e) a)))
      ("cdr"   (cdr  (eval (second e) a)))
      ("cons"  (cons (eval (second e) a)
                     (eval (third e) a)))
      ("cond"  (eval-cond (cdr e) a))
      ("else"  (eval (cons (assoc (car e) a) (cdr e))
                   a))))
   (else
    (case (car (car e))
      ("label"  (eval (cons (car (cdr (cdr (car e)))) (cdr e))
                      (cons (list (car (cdr (car e))) (car e)) a)))
      ("lambda" (eval (third (car e))
                      (append (pair (second (car e)) (eval-list (cdr e) a))
                              a)))))))

(define (eval-cond c a)
  (if (equal? '() (eval (car (car c)) a))
      (eval-cond (cdr c) a)
      (eval-cond (second (car c)) a)))

(define (eval-list m a)
  (if (null? m)
      '()
      (cons (eval      (car m) a)
            (eval-list (cdr m) a))))
