(in-package :netfarm-slacker)

(defun setup-slacker-table ()
  (let ((table (copy-readtable nil)))
    (set-macro-character #\[
                         (lambda (stream char)
                           (declare (ignore char))
                           (read-delimited-list #\] stream t))
                         nil table)
    (set-macro-character #\] (get-macro-character #\) table)
                         nil table)
    (set-dispatch-macro-character #\# #\t (constantly :true) table)
    (set-dispatch-macro-character #\# #\f (constantly :false) table)
    table))

(defun read-slacker (stream &rest rest)
  (let ((*readtable* (setup-slacker-table))
        (*read-eval* nil)
        (*package* (find-package :netfarm-slacker-symbols)))
    (apply #'read stream rest)))

(defun read-all (stream)
  (when (stringp stream)
    (setf stream (make-string-input-stream stream)))
  (loop collect (handler-case (read-slacker stream)
                  (end-of-file ()
                    (loop-finish)))))
        
