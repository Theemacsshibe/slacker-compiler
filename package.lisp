(defpackage :netfarm-slacker-symbols
  (:import-from :cl
                #:if #:cond
                #:let #:let*
                #:lambda #:quote
                #:+ #:- #:* #:/
                #:< #:<= #:= #:>= #:>
                #:car #:cdr #:cons)
  (:export #:if #:cond
           #:let #:let* #:lambda
           #:begin #:set! #:quote
           #:define #:define-macro
           #:define-method
           #:+ #:- #:* #:/
           #:< #:<= #:= #:>= #:>
           #:car #:cdr #:cons))

(defpackage :netfarm-slacker
  (:use :cl :netfarm :netfarm-slacker-symbols)
  (:export #:read-slacker #:read-all
           #:compile-program
           #:compile-program-from-file
           #:compile-program-from-string))
