(in-package :netfarm-slacker)

(defun translate-constant (value)
  (etypecase value
    (list   (mapcar #'translate-constant value))
    ((or string integer (member :true :false)) value)
    (vector (coerce value '(vector (unsigned-byte 8))))
    (symbol (string-downcase value))))
