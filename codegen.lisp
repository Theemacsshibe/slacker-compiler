(in-package :netfarm-slacker)

(defgeneric generate-cons-code (name arguments))
(defmacro define-codegen ((name &rest arguments) &body body)
  `(defmethod generate-cons-code ((name (eql ',name)) arguments)
     (destructuring-bind ,arguments arguments
       ,@body)))

(defgeneric generate-code (code)
  (:method ((cons cons))
    (generate-cons-code (first cons) (rest cons))))

(define-codegen (call function &rest arguments)
  (destructuring-bind (function-type &rest description)
      function
    (emit-call function-type description arguments nil)))
(define-codegen (tailcall function &rest arguments)
  (destructuring-bind (function-type &rest description)
      function
    (emit-call function-type description arguments t)))

(defgeneric emit-call (function-type function-description arguments tail-call?)
  (:method ((|the symbol primop| (eql 'primop)) desc arguments tail-call?)
    (destructuring-bind (name) desc
      (let ((primop (find-primop name)))
        (append (emit-primop-instruction primop
                                         arguments)
                (if (primop-pushes-value-p primop)
                    '()
                    (list (primop-named byte) 0))
                (if tail-call?
                    (list (primop-named return))
                    '())))))
  (:method (functional-place description arguments tail-call?)
    (append (reduce #'append (mapcar #'generate-code arguments)
                    :from-end t)
            (generate-code (cons functional-place description))
            (list (if tail-call?
                      (primop-named tail-call)
                      (primop-named call))
                  (length arguments)))))

(define-codegen (byte n)
  (list (primop-named byte) n))

(define-codegen (begin &rest expressions)
  ;; For (begin e1 ... en) generate C[e1] drop C[e2] drop ... C[en]
  (loop for (expression . rest) on expressions
        append (generate-code expression)
        unless (null rest)
          append (list (primop-named drop))))

(define-codegen (function n)
  (list (primop-named get-proc) n))
(define-codegen (function* n)
  (list (primop-named get-proc*) n))

(define-codegen (global n)
  (list (primop-named get-value) n))
(define-codegen (set-global! n value)
  (append (generate-code value)
          (list (primop-named set-value!) n)))

(define-codegen (local position depth)
  (list (primop-named get-env) position depth))
(define-codegen (set-local! position depth value)
  (append (generate-code value)
          (list (primop-named set-env!) position depth)))

(define-codegen (set! place value)
  (let ((new-name (ecase (first place)
                    ((local)  'set-local!)
                    ((global) 'set-global!))))
    (append (generate-code `(,new-name ,@(rest place) ,value))
            (list (primop-named byte) 0))))

(define-codegen (return form)
  (append (generate-code form)
          (list (primop-named return))))

(defun split-word (word)
  (values (ldb (byte 8 8) word)
          (ldb (byte 8 0) word)))

(define-codegen (if test then else)
  (let* ((then-code (generate-code then))
         (else-code (generate-code else))
         (then-length (length then-code))
         (else-length (length else-code))
         (then-segment-length (+ 3 then-length)))
    (multiple-value-bind (then-high then-low)
        (split-word then-segment-length)
      (multiple-value-bind (else-high else-low)
          (split-word else-length)
      (append (generate-code test)
              (list (primop-named jump-cond)
                    0 0
                    then-high then-low)
              (generate-code then)
              (list (primop-named jump)
                    else-high else-low)
              (generate-code else))))))
