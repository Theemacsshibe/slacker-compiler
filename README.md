A Scheme-like language for the Netfarm script machine. Notable for having a
very lazy implementation, and generally just being only somewhat nicer than
writing SECD assembly.


## Input language

```scheme
toplevel   ::= (define <symbol> (quote <datum>))
            |  (define <symbol> (lambda (<symbol>*) <expression>))
            |  (define (<symbol> <symbol>*) <expression>*)
todo:       |  (use <pathname>*)
            |  (define-macro (<symbol> <symbol>*) <expression>)

expression ::= (quote <datum>)
            |  (lambda (<symbol>*) <expression>)
            |  (set! <symbol> <expression>)
            |  (if <expression> <expression> <expression>)
            |  (begin <expression> <expression>*)
            |  (<expression> <expression*>)
            |  <symbol>
            |  <self-evaluating-datum>
            
datum                 ::= <symbol> | <self-evaluating-datum>
self-evaluating-datum ::= <byte-vector>
                       |  <integer>
                       |  <string>
byte-vector ::= #(<byte>*)
```

(`let` and `letrec` can be derived from `lambda` and `set!` without much of a
 hassle. `cond` can be derived from `if`; `cond` is historically the base 
 conditional operator, but it's apparently cooler to make `if` the base now.)

## Intermediate language

The intermediate language concerns itself with `expression`-position forms in
the previous language; and every function and global has been lifted to a
global function or variable. Primops in an argument position are also lifted.
Tail positions have also been explicated, with explicit `tailcall`s and 
`return`s.

```scheme
function   ::= (primop <name>)
            |  (function <address>)
            |  (function* <address>)

expression ::= (global <address>) 
            |  (function <address>) | (function* <address>)
            |  (byte <byte>)
            |  (local <frame> <position>)
            |  (if <expression> <expression> <expression>)
            |  (call <function> <expression>*)
            |  (tailcall <function> <expression>*)
            |  (return <expression>)
            |  (begin <expression> <expression>*)
            |  (set! <address> <expression>)
```

## Benchmarks

Remember that only one of these language implementations has to run
untrusted code and in a deterministic manner all the time.

We test an implementation of the Ackermann function, calling *A*(3, 3).
No optimisation settings were used.

| Language    | Implementation | Microseconds | Numbers are |
|:------------|:---------------|-------------:|:------------|
| Lua         | LuaJIT 2.0.5   |          7.8 | floats      |
| Common Lisp | SBCL 2.1.1     |         14.7 | fix/bignums |
| Lua         | Lua 5.4.2      |        102.7 | floats      |
| Common Lisp | CLISP 2.49.93  |        121.2 | fix/bignums |
| Python 3    | CPython 3.9.2  |        220.3 | bignums     |
| Slacker     | Netfarm 0.1.1* |        385.2 | fix/bignums |
| idk         | Some toy VM    |        958.0 | floats      |
| idk^2       | Ditto          |       3415.5 | mod 2^32    |

Could be worse, could be better.

\* We used the fcfde62 commit of Netfarm, and SBCL 2.1.1.
