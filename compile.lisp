(in-package :netfarm-slacker)

(defun allocate-names (program)
  (dolist (definition program)
    (trivia:ematch definition
      ((list 'define-method (list* name _) _)
       (let ((new-name (gensym "METHOD")))
         (setf (gethash (string-downcase name) *methods*) new-name)
         (allocate-function-binding new-name)))
      ((list* 'define (list* function _) _)
       (allocate-function-binding function))
      ((list 'define function (list* 'lambda _ _))
       (allocate-function-binding function))
      ((list* 'define-macro (cons function arguments) body)
       (add-macro function arguments body))
      ((list 'define global _)
       (allocate-global-binding global)))))

(defun emit-code (function-table function-names)
  (loop with codes = (make-array (length function-names))
        for name across function-names
        for position from 0
        for (argument-list environment code)
          = (gethash name function-table)
        do (setf (aref codes position)
                 (list (length argument-list)
                       (generate-code code)))
           #+cool-looking-ir-printout
           (format t "~3d. ~12@a: ~s~%" position name code)
        finally (return codes)))

(defun globals (global-names global-table)
  (loop with globals = (make-array (length global-names))
        for name across global-names
        for position from 0
        for value = (gethash name global-table)
        do (setf (aref globals position) value)
        finally (return (coerce globals 'list))))

(defun methods (method-table entry-points function-environment)
  (loop for method-name   being the hash-keys of method-table
        for function-name being the hash-values of method-table
        for position = (position function-name function-environment)
        for (start argument-count) = (nth position entry-points)
        collect (list method-name start (1- argument-count))))

(defun generate-program-data (codes)
  (loop with program-length = (reduce #'+
                                      (map 'vector #'second codes)
                                      :key #'length)
        with program        = (make-array program-length
                                          :element-type '(unsigned-byte 8))
        with position       = 0
        for (argument-count bytecode) across codes
        do (replace program bytecode :start1 position)
        collect (list position argument-count) into entry-points
        do (incf position (length bytecode))
        finally (return (values entry-points program))))

(defun compile-program (program)
  (with-global-environments ()
    (allocate-names program)
    (mapc #'flatten-lambdas/toplevel program)
    (let ((codes (emit-code *functions* *function-environment*)))
      (multiple-value-bind (entry-points program)
          (generate-program-data codes)
        (make-instance 'netfarm-scripts:script
                       :entry-points entry-points
                       :program program
                       :methods (methods *methods* entry-points
                                         *function-environment*)
                       :variables (globals *global-environment*
                                           *globals*))))))

(defun compile-program-from-file (pathname)
  (with-open-file (s pathname
                     :if-does-not-exist :error
                     :external-format :utf-8)
    (compile-program (read-all s))))

(defun compile-program-from-string (string)
  (with-input-from-string (s string)
    (compile-program (read-all s))))
